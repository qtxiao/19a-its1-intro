# Exercises for ww36


## Exercise 1 - Kompetence selvevaluering

### Information

Vi har 6 niveauer.

* Niveau 0: Intet kendskab til fagområdet
* Niveau 1: ”Novice niveau”

  Perifert kendskab til et fagområde. Har en vag fornemmelse af, hvad der er væsentligt inden for fagområdet.

  Kan ikke selvstændigt (eller kun meget vanskeligt) udføre opgaver inden
  for fagområdet

* Niveau 2: ”Generalist niveau”

  Kan med passende støtte udføre standardopgaver inden for fagområdet, men har ikke overblik over arbejdsprocessen.

  Personen ved, at der er mange ting, som pågældende ikke kender til inden for fagområdet.

  Kan i konkrete situationer genkende hvad der behøves hjælp til, når personen står lige overfor et problem.

* Karakter 3: ”Håndværker niveau”

  Kan lave det meste arbejde selv, når der er tale om standardopgaver, men har løbende brug for støtte, når der er tale om mere komplekse opgaver, som ikke er ”lige efter bogen”.

  Kender arbejdsprocessen i praksis, men kan næppe beskrive den endnu overfor andre.

* Karakter 4: ”Specialist niveau”

  Det teoretiske fundament for at forstå fagområdet er ved at blive skabt.

  Kan selvstændigt tilegne sig viden og analysere sig frem til metoder, så også mere komplekse problemstillinger og opgaver kan løses. Dog haves endnu ikke den nødvendige færdighed til at kunne løse alle opgaver uden videre (det ”rystes ikke bare ud af ærmet”).

  Har et godt overblik over fagområdet, så personen præcis kan redegøre for, hvad pågældende ikke ved og hvordan denne viden om nødvendigt kan skaffes.

* Niveau 5: ”Ekspert niveau”

  Det teoretiske fundament er på plads inden for fagområdet, og har et tilstrækkeligt teoretisk og praktisk overblik over fagområdet (meta forståelse) til at kunne undervise andre heri.

  Kan fremhæve alle væsentlige områder inden for fagområdet.

  Kan deltage i kvalificerede diskussioner med andre eksperter uden at ”falde igennem”.

  Kan løse alle standard og komplekse opgaver inden for fagområdet uden at skulle søge væsentlig viden udefra. Evt. er helt specifikke detaljer ikke 100% present i hukommelsen, men personen vil da præcis vide hvor og hvordan disse detaljer kan findes hurtigt og effektivt.

### Exercise instructions

1. Download excel arket fra [repo](https://gitlab.com/uct-its-19/19a-its1-intro/raw/master/Niveauer/niveau.xlsx?inline=false) og gem det som `niveau_<ident>.xlsx`
2. Rate dig selv med en score på 0-5 på alle emnerne.
3. Upload til gitlab på [https://gitlab.com/uct-its-19/19a-its1-intro/tree/master/Niveauer](https://gitlab.com/uct-its-19/19a-its1-intro/tree/master/Niveauer)

### Links
No links
