---
title: 'ITS1 Introduktion til IT sikkerhed'
subtitle: 'Exercises'
#authors: ['Nikolaj Simonsen \<nisi@ucl.dk\>', 'Ilias Esmati \<iles@ucl.dk\>']
main_author: 'Morten Bo Nielsen'
date: \today
email: 'mon@ucl.dk'
left-header: \today
right-header: 'ITS1 Intro, exercises'
---


Introduction
====================

This document is a collection of exercises. They are associated with the weekly plans.

References

* [Weekly plans](https://ucl-pba-its.gitlab.io/19a-its1-intro/19A_ITS1_intro_weekly_plans.pdf)
